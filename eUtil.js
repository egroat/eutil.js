/* Copyright Eoin Groat
 * Licensed with the UNLICENSE license:
 *  http://unlicense.org/
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org/>
 */

'use strict';

function eUtil() {}

// Converts array-like or falsey objects to arrays
// Useful for querySelector results
eUtil.prototype.asArray = function(obj) {
  if(!obj)
    return []
  return [].slice.call(obj)
}

// Add a class to a DOM element
eUtil.prototype.setClass = function(clss, element) {
  if(element instanceof Array)
    return element.map(setClass.bind(this, clss))

  var list = asArray(element.classList)
  if(list.indexOf(clss) >= 0)
    return undefined

  list.push(clss)
  element.className = list.join(' ')
  return element
}

// Remove a class from a DOM element
eUtil.prototype.removeClass = function(clss, element) {
  if(element instanceof Array)
    return element.map(removeClass.bind(this, clss))

  var list  = asArray(element.classList)
    , index = list.indexOf(clss)

  if(index < 0)
    return undefined
  
  do {
    delete list[index]
    index = list.indexOf(clss)
  } while(index >= 0)
  
  element.className = list.join(' ')
  return element
}

// DOMReady,  add a function to be called when the DOM is ready
// Taken and edited from a stack overflow answer:
//   http://stackoverflow.com/questions/9899372#9899701
// Also on github:
//   https://github.com/jfriend00/docReady
eUtil.prototype.DOMReady = (function() {
  var list = []
    , fired = false
    , eventHandlersInstalled = false

  function ready() {
    if(fired)
      return
    fired = true
    list.forEach(function(i) {
      i.f.call(window, i.ctx)
    })
    list = []
  }

  function stateChange() {
    if(document.readyState === "complete")
      ready()
  }

  return function(f, ctx) {
    if(fired)
      return setTimeout(function() {f(ctx)}, 1)
    list.push({f: f, ctx: ctx})
    
    if(document.readyState === "complete" ||
        (!document.attachEvent && document.readyState === "interactive"))
      return setTimeout(ready, 1)

    if(eventHandlersInstalled)
      return

    if(document.addEventListener) {
      document.addEventListener("DOMContentLoaded", ready, false)
      window.addEventListener("load", ready, false)
    } else {
      document.attachEvent("onreadystatechange", readyStateChange)
      window.attachEvent("onload", ready)
    }

    eventHandlersInstalled = true
  }
})(this)
